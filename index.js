function sum( a,  b) {
    // chuyển 2 chuỗi a,b thành array, ví dụ : "12" -> ["1","2"]
    var arrA = a.split(',');
    var arrB = b.split(',');
    var arrResult = [];

    // nếu số a (hoặc b) khác độ dài, ví dụ (a = 11, b = 111) -> chuyển đổi a = 011
    if (arrA.length > arrB.length) {
      while (arrA.length - arrB.length !== 0) {
        arrB = ['0', ...arrB];
      }
    } else {
      while (arrB.length - arrA.length !== 0) {
        arrA = ['0', ...arrA];
      }
    }

    // (1+9 = 10, viết 0, nhớ 1 => point này là phần nhớ)
    var point = 0;
    var arrayleng = arrA.length - 1
    for (var i = arrayleng; i >= 0; i--) {
      if (i !== 0) {
        //chuyển về 2 số dạng int
        const numA = Number(arrA[i]);
        const numB = Number(arrB[i]);
        const numAB = numA + numB + point;
        if (numAB >= 10) {
          //nếu 2 số cộng lại lớn hơn 10 thì nhớ 1
          point = 1;

          //lấy phần dư
          const numABsaved = numAB % 10;

          //thêm vào mảng kết quả
          arrResult = [numABsaved.toString(), ...arrResult];
        } else {
          //nếu nhỏ hơn 10 thì ko làm gì hết, chỉ thêm vào mảng kết quả
          arrResult = [numAB.toString(), ...arrResult];
          point = 0;
        }
      } else {
        // vì 2 số cuối cùng cộng lại, xử lí khác nên phải tách ra trường hợp riêng
        const numA = Number(arrA[0]);
        const numB = Number(arrB[0]);
        const numAB = numA + numB + point;
        if (numAB >= 10) {
          //nếu cộng lại lớn hon 10 thì tách số đó ra làm phần ngueyen và dư, sau đó thêm vào mảng kết quả
          const numAB1 = numAB % 10;
          const numAB2 =  Math.floor(numAB/10);
          arrResult = [numAB2.toString(), numAB1.toString(), ...arrResult];
        } else {
          //nếu nhỏ hơn 10 thì chỉ cần cộng lại
          arrResult = [numAB.toString(), ...arrResult];
        }
      }
    }
    //kết quả cuối cùng ["1","2","3"] -> .join() => "123"
    const numAfterPlus= arrResult.join('');

    return numAfterPlus;
  }
  
var a = '12', b = '2'
console.log(sum(a,b))
